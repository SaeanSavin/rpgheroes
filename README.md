# Background
RpgHeroes is a mandatory assignment at Noroff Accelerate. This assignment is coded in C# and have xUnit tests 


# Rpg Heroes
RpgHeroes is character creator program. You can create different heroes and equip them with weapons and armor. Print out hero stats and see it changes when equipping different armor. 

and includes tests for different scenarios. 


## Requirements
```
 - .Net 6.0
```

## Install

#### Clone repository and compile:
 Clone project and make sure .Net 6.0 is installed.
 Use this command to compile
```
 - dotnet build --no-restore
```

## Tests 
Use this command to run all tests:
```
 - dotnet test --no-restore
```
