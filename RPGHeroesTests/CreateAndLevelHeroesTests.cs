using RPGHeroes.Attributes;
using RPGHeroes.Entities;
using RPGHeroes.Entities.Heroes;
using RPGHeroes.Exception;
using RPGHeroes.Items;
using Xunit.Sdk;

namespace RPGHeroesTests
{
    public class CreateAndLevelHeroesTests
    {
        #region Default Hero stats

        [Fact]
        public void Hero_CreateHeroWithName_ShouldReturnHeroName()
        {
            string heroName = "Frank";
            string expected = heroName;

            Warrior warrior = new(heroName);
            string actual = warrior.Name;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Hero_CreateLevel1Hero_ShouldReturn1()
        {
            string heroName = "Frank";
            int expected = 1;

            Warrior warrior = new(heroName);
            int actual = warrior.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Hero_CreateDefaultHeroAttribute_ShouldReturnStrengthAttribute()
        {
            string heroName = "Frank";
            int expected = 5;

            Warrior warrior = new(heroName);
            int actual = warrior.HeroAttributes.Strength;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Hero_CreateDefaultHeroAttribute_ShouldReturnDexterityAttribute()
        {
            string heroName = "Frank";
            int expected = 2;

            Warrior warrior = new(heroName);
            int actual = warrior.HeroAttributes.Dexterity;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Hero_CreateDefaultHeroAttribute_ShouldReturnIntelligenceAttribute()
        {
            string heroName = "Frank";
            int expected = 1;

            Warrior warrior = new(heroName);
            int actual = warrior.HeroAttributes.Intelligence;

            Assert.Equal(expected, actual);
        }

        #endregion

        #region LevelUp Mage

        [Fact]
        public void LevelUp_Mage_ShouldReturnLevel2()
        {
            string name = "Gandalf";
            int level = 2;
            int expected = level;

            Mage mage = new(name);
            mage.LevelUp();

            int actual = mage.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_Mage_ShouldReturnLevel2StrengthAttribute()
        {
            string name = "Gandalf";
            int strengthAttribute = 2;
            int expected = strengthAttribute;

            Mage mage = new(name);
            mage.LevelUp();

            int actual = mage.HeroAttributes.Strength;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_Mage_ShouldReturnLevel2DexterityAttribute()
        {
            string name = "Gandalf";
            int dexterityAttribute = 2;
            int expected = dexterityAttribute;

            Mage mage = new(name);
            mage.LevelUp();

            int actual = mage.HeroAttributes.Dexterity;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_Mage_ShouldReturnLevel2IntelligenceAttribute()
        {
            string name = "Gandalf";
            int intelligenceAttribute = 13;
            int expected = intelligenceAttribute;

            Mage mage = new(name);
            mage.LevelUp();

            int actual = mage.HeroAttributes.Intelligence;

            Assert.Equal(expected, actual);
        }

        #endregion

        #region LevelUp Ranger

        [Fact]
        public void LevelUp_Ranger_ShouldReturnLevel2()
        {
            string name = "Bow man";
            int level = 2;
            int expected = level;

            Ranger ranger = new(name);
            ranger.LevelUp();

            int actual = ranger.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_Ranger_ShouldReturnLevel2StrengthAttribute()
        {
            string name = "Bow man";
            int strengthAttribute = 2;
            int expected = strengthAttribute;

            Ranger ranger = new(name);
            ranger.LevelUp();

            int actual = ranger.HeroAttributes.Strength;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_Ranger_ShouldReturnLevel2DexterityAttribute()
        {
            string name = "Bow man";
            int dexterityAttribute = 12;
            int expected = dexterityAttribute;

            Ranger ranger = new(name);
            ranger.LevelUp();

            int actual = ranger.HeroAttributes.Dexterity;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_Ranger_ShouldReturnLevel2IntelligenceAttribute()
        {
            string name = "Bow man";
            int intelligenceAttribute = 2;
            int expected = intelligenceAttribute;

            Ranger ranger = new(name);
            ranger.LevelUp();

            int actual = ranger.HeroAttributes.Intelligence;

            Assert.Equal(expected, actual);
        }

        #endregion

        #region LevelUp Rogue

        [Fact]
        public void LevelUp_Rogue_ShouldReturnLevel2()
        {
            string name = "Rouge";
            int level = 2;
            int expected = level;

            Rogue rogue = new(name);
            rogue.LevelUp();

            int actual = rogue.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_Rogue_ShouldReturnLevel2StrengthAttribute()
        {
            string name = "Rouge";
            int strengthAttribute = 3;
            int expected = strengthAttribute;

            Rogue rogue = new(name);
            rogue.LevelUp();

            int actual = rogue.HeroAttributes.Strength;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_Rogue_ShouldReturnLevel2DexterityAttribute()
        {
            string name = "Rogue";
            int dexterityAttribute = 10;
            int expected = dexterityAttribute;

            Rogue rogue = new(name);
            rogue.LevelUp();

            int actual = rogue.HeroAttributes.Dexterity;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_Rogue_ShouldReturnLevel2IntelligenceAttribute()
        {
            string name = "Rogue";
            int intelligenceAttribute = 2;
            int expected = intelligenceAttribute;

            Rogue rogue = new(name);
            rogue.LevelUp();

            int actual = rogue.HeroAttributes.Intelligence;

            Assert.Equal(expected, actual);
        }

        #endregion

        #region LevelUp Warrior

        [Fact]
        public void LevelUp_Warrior_ShouldReturnLevel2()
        {
            string name = "Frank";
            int level = 2;
            int expected = level;

            Warrior warrior = new(name);
            warrior.LevelUp();

            int actual = warrior.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_Warrior_ShouldReturnLevel2StrengthAttribute()
        {
            string name = "Frank";
            int strengthAttribute = 8;
            int expected = strengthAttribute;

            Warrior warrior = new(name);
            warrior.LevelUp();

            int actual = warrior.HeroAttributes.Strength;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_Warrior_ShouldReturnLevel2DexterityAttribute()
        {
            string name = "Frank";
            int dexterityAttribute = 4;
            int expected = dexterityAttribute;

            Warrior warrior = new(name);
            warrior.LevelUp();

            int actual = warrior.HeroAttributes.Dexterity;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_Warrior_ShouldReturnLevel2IntelligenceAttribute()
        {
            string name = "Frank";
            int intelligenceAttribute = 2;
            int expected = intelligenceAttribute;

            Warrior warrior = new(name);
            warrior.LevelUp();

            int actual = warrior.HeroAttributes.Intelligence;

            Assert.Equal(expected, actual);
        }

        #endregion
    }

    public class WeaponAndArmorTests
    {
        #region Weapon
        [Fact]
        public void Weapon_CreateDefaultWeapon_ShouldReturnWeaponName()
        {
            string weaponName = "Common Axe";
            int levelRequirement = 1;
            ItemSlot weaponSlot = ItemSlot.Weapon;
            WeaponType weaponType = WeaponType.Axe;
            double weaponDamage = 2;

            string expected = weaponName;

            Weapon weapon = new(weaponName, levelRequirement, weaponSlot, weaponType, weaponDamage);
            string? actual = weapon.Name;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Weapon_CreateDefaultWeapon_ShouldReturnWeaponLevelRequirement()
        {
            string weaponName = "Common Axe";
            int levelRequirement = 1;
            ItemSlot weaponSlot = ItemSlot.Weapon;
            WeaponType weaponType = WeaponType.Axe;
            double weaponDamage = 2;

            int expected = levelRequirement;

            Weapon weapon = new(weaponName, levelRequirement, weaponSlot, weaponType, weaponDamage);
            int actual = weapon.RequiredLevel;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Weapon_CreateDefaultWeapon_ShouldReturnItemSlot()
        {
            string weaponName = "Common Axe";
            int levelRequirement = 1;
            ItemSlot weaponSlot = ItemSlot.Weapon;
            WeaponType weaponType = WeaponType.Axe;
            double weaponDamage = 2;

            ItemSlot expected = weaponSlot;

            Weapon weapon = new(weaponName, levelRequirement, weaponSlot, weaponType, weaponDamage);
            ItemSlot actual = weapon.Slot;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Weapon_CreateDefaultWeapon_ShouldReturnWeaponType()
        {
            string weaponName = "Common Axe";
            int levelRequirement = 1;
            ItemSlot weaponSlot = ItemSlot.Weapon;
            WeaponType weaponType = WeaponType.Axe;
            double weaponDamage = 2;

            WeaponType expected = weaponType;

            Weapon weapon = new(weaponName, levelRequirement, weaponSlot, weaponType, weaponDamage);
            WeaponType actual = weapon.WeaponType;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Weapon_CreateDefaultWeapon_ShouldReturnWeaponDamage()
        {
            string weaponName = "Common Axe";
            int levelRequirement = 1;
            ItemSlot weaponSlot = ItemSlot.Weapon;
            WeaponType weaponType = WeaponType.Axe;
            double weaponDamage = 2;

            double expected = weaponDamage;

            Weapon weapon = new(weaponName, levelRequirement, weaponSlot, weaponType, weaponDamage);
            double actual = weapon.WeaponDamage;

            Assert.Equal(expected, actual);
        }

        #endregion

        #region Armor

        [Fact]
        public void Armor_CreateDefaultArmor_ShouldReturnArmorName()
        {
            string armorName = "Common Cloth";
            int levelRequirement = 1;
            ItemSlot slot = ItemSlot.Weapon;
            ArmorType armorType = ArmorType.Cloth;
            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;

            HeroAttribute armorAttribute = new(armorStrengthAttribute, armorDexterityAttribute, armorIntelligenceAttribute);

            string expected = armorName;

            Armor armor = new(armorName, levelRequirement, slot, armorType, armorAttribute);
            string? actual = armor.Name;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Armor_CreateDefaultArmor_ShouldReturnArmorLevelRequirement()
        {
            string armorName = "Common Cloth";
            int levelRequirement = 1;
            ItemSlot slot = ItemSlot.Weapon;
            ArmorType armorType = ArmorType.Cloth;
            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;

            HeroAttribute armorAttribute = new(armorStrengthAttribute, armorDexterityAttribute, armorIntelligenceAttribute);

            int expected = levelRequirement;

            Armor armor = new(armorName, levelRequirement, slot, armorType, armorAttribute);
            int actual = armor.RequiredLevel;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Armor_CreateDefaultArmor_ShouldReturnArmorSlot()
        {
            string armorName = "Common Cloth";
            int levelRequirement = 1;
            ItemSlot slot = ItemSlot.Weapon;
            ArmorType armorType = ArmorType.Cloth;
            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;

            HeroAttribute armorAttribute = new(armorStrengthAttribute, armorDexterityAttribute, armorIntelligenceAttribute);

            ItemSlot expected = slot;

            Armor armor = new(armorName, levelRequirement, slot, armorType, armorAttribute);
            ItemSlot actual = armor.Slot;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Armor_CreateDefaultArmor_ShouldReturnArmorType()
        {
            string armorName = "Common Cloth";
            int levelRequirement = 1;
            ItemSlot slot = ItemSlot.Weapon;
            ArmorType armorType = ArmorType.Cloth;
            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;

            HeroAttribute armorAttribute = new(armorStrengthAttribute, armorDexterityAttribute, armorIntelligenceAttribute);

            ArmorType expected = armorType;

            Armor armor = new(armorName, levelRequirement, slot, armorType, armorAttribute);
            ArmorType actual = armor.ArmorType;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Armor_CreateDefaultArmor_ShouldReturnArmorAttribute()
        {
            string armorName = "Common Cloth";
            int levelRequirement = 1;
            ItemSlot slot = ItemSlot.Weapon;
            ArmorType armorType = ArmorType.Cloth;
            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;

            HeroAttribute armorAttribute = new(armorStrengthAttribute, armorDexterityAttribute, armorIntelligenceAttribute);

            HeroAttribute expected = armorAttribute;

            Armor armor = new(armorName, levelRequirement, slot, armorType, armorAttribute);
            HeroAttribute? actual = armor.ArmorAttribute;

            Assert.Equal(expected, actual);
        }

        #endregion

        #region Equip Weapon
        
        [Fact]
        public void EquipWeapon_EquipSword_ShouldReturnHeroWithWeaponEquipped()
        {
            string name = "Frank";

            string weaponName = "Common Sword";
            int requiredLevel = 1;
            ItemSlot slot = ItemSlot.Weapon;
            WeaponType weaponType = WeaponType.Sword;
            int weaponDamage = 2;

            Weapon commonSword = new(weaponName, requiredLevel, slot, weaponType, weaponDamage);

            Dictionary<ItemSlot, Item?> expected = new() { {slot, commonSword } };

            Warrior warrior = new(name);
            warrior.Equip(commonSword);

            var actual = warrior.Equipment;

            Assert.Equal(expected[ItemSlot.Weapon], actual[ItemSlot.Weapon]);
        }

        [Fact]
        public void EquipWeapon_EquipInvalidLevelRequirement_ShouldThrowInvalidWeaponException()
        {
            string name = "Frank";

            string weaponName = "Common Sword";
            int requiredLevel = 2;
            ItemSlot slot = ItemSlot.Weapon;
            WeaponType weaponType = WeaponType.Sword;
            int weaponDamage = 2;

            Weapon commonSword = new(weaponName, requiredLevel, slot, weaponType, weaponDamage);
            Warrior warrior = new(name);

            Assert.Throws<InvalidWeaponException>(() => warrior.Equip(commonSword));
        }

        [Fact]
        public void EquipWeapon_EquipInvalidType_ShouldThrowInvalidWeaponException()
        {
            string name = "Frank";

            string weaponName = "Common Bow";
            int requiredLevel = 1;
            ItemSlot slot = ItemSlot.Weapon;
            WeaponType weaponType = WeaponType.Bow;
            int weaponDamage = 2;

            Weapon commonBow = new(weaponName, requiredLevel, slot, weaponType, weaponDamage);
            Warrior warrior = new(name);

            Assert.Throws<InvalidWeaponException>(() => warrior.Equip(commonBow));
        }

        #endregion

        #region Equip Armor

        [Fact]
        public void EquipArmor_EquipValidArmor_ShouldEquipArmor()
        {
            string heroName = "Frank";

            string armorName = "Common Chest Plate";
            int requiredLevel = 1;
            ItemSlot slot = ItemSlot.Chest;
            ArmorType armorType = ArmorType.Plate;

            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;

            HeroAttribute armorAttribute = new(
                armorStrengthAttribute, 
                armorDexterityAttribute, 
                armorIntelligenceAttribute
            );
            Armor chestPlate = new(armorName, requiredLevel, slot, armorType, armorAttribute);

            Dictionary<ItemSlot, Item?> expected = new()
            {
                { slot, chestPlate }
            };

            Warrior warrior = new(heroName);
            warrior.Equip(chestPlate);

            var actual = warrior.Equipment;

            Assert.Equal(expected[slot], actual[slot]);
        }

        [Fact]
        public void EquipArmor_EquipInvalidLevelRequirement_ShouldThrowInvalidArmorException()
        {
            string heroName = "Frank";

            string armorName = "Common Chest Plate";
            int requiredLevel = 2;
            ItemSlot slot = ItemSlot.Chest;
            ArmorType armorType = ArmorType.Plate;

            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;
            HeroAttribute armorAttribute = new(
                armorStrengthAttribute, 
                armorDexterityAttribute, 
                armorIntelligenceAttribute
            );

            Warrior warrior = new(heroName);

            Armor chestPlate = new(armorName, requiredLevel, slot, armorType, armorAttribute);
            Assert.Throws<InvalidArmorException>(() => warrior.Equip(chestPlate));
        }

        [Fact]
        public void EquipArmor_EquipInvalidType_ShouldThrowInvalidArmorException()
        {
            string heroName = "Frank";

            string armorName = "Common Hood";
            int requiredLevel = 1;
            ItemSlot slot = ItemSlot.Head;
            ArmorType armorType = ArmorType.Leather;

            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;

            HeroAttribute armorAttribute = new(
                armorStrengthAttribute, 
                armorDexterityAttribute, 
                armorIntelligenceAttribute
            );

            Warrior warrior = new(heroName);

            Armor chestPlate = new(armorName, requiredLevel, slot, armorType, armorAttribute);
            Assert.Throws<InvalidArmorException>(() => warrior.Equip(chestPlate));
        }

        #endregion
    }

    public class TotalAttributeTests
    {
        [Fact]
        public void TotalAttribute_NoEquipment_ShouldReturnHeroAttribute()
        {
            string heroName = "Frank";
            int strength = 5;
            int dexterity = 2;
            int intelligence = 1;

            var expected = new HeroAttribute(strength, dexterity, intelligence);

            Warrior warrior = new(heroName);

            var actual = warrior.TotalAttributes();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TotalAttribute_OneArmor_ShouldReturnHeroAttributePlusEquipmentAttribute()
        {
            string heroName = "Frank";

            string armorName = "Common Chest Plate";
            int requiredLevel = 1;
            ItemSlot slot = ItemSlot.Chest;
            ArmorType armorType = ArmorType.Plate;
            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;

            HeroAttribute armorAttribute = new(
                armorStrengthAttribute, 
                armorDexterityAttribute, 
                armorIntelligenceAttribute
            );
            Warrior warrior = new(heroName);
            var expected = (warrior.HeroAttributes + armorAttribute);

            Armor chestPlate = new(armorName, requiredLevel, slot, armorType, armorAttribute);
            warrior.Equip(chestPlate);

            var actual = warrior.TotalAttributes();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TotalAttribute_TwoArmor_ShouldReturnHeroAttributePlusEquipmentAttribute()
        {
            string heroName = "Frank";

            string armorName = "Common Chest Plate";
            int requiredLevel = 1;
            ItemSlot slot = ItemSlot.Chest;
            ArmorType armorType = ArmorType.Plate;
            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;
            HeroAttribute armorAttribute = new(
                armorStrengthAttribute, 
                armorDexterityAttribute, 
                armorIntelligenceAttribute
            );

            string armorName2 = "Common Head Plate";
            int requiredLevel2 = 1;
            ItemSlot slot2 = ItemSlot.Head;
            ArmorType armorType2 = ArmorType.Plate;
            int armorStrengthAttribute2 = 1;
            int armorDexterityAttribute2 = 1;
            int armorIntelligenceAttribute2 = 0;
            HeroAttribute armorAttribute2 = new(
                armorStrengthAttribute2, 
                armorDexterityAttribute2, 
                armorIntelligenceAttribute2
            );

            Warrior warrior = new(heroName);
            var expected = (warrior.HeroAttributes + armorAttribute + armorAttribute2);

            Armor chestPlate = new(armorName, requiredLevel, slot, armorType, armorAttribute);
            Armor hoodPlate = new(armorName2, requiredLevel2, slot2, armorType2, armorAttribute2);
            warrior.Equip(chestPlate);
            warrior.Equip(hoodPlate);

            var actual = warrior.TotalAttributes();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TotalAttribute_ReplaceArmor_ShouldReturnUpdatedTotalAttribute()
        {
            string heroName = "Frank";

            string armorName = "Common Chest Plate";
            int requiredLevel = 1;
            ItemSlot slot = ItemSlot.Chest;
            ArmorType armorType = ArmorType.Plate;
            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 1;
            int armorIntelligenceAttribute = 0;
            HeroAttribute armorAttribute = new(
                armorStrengthAttribute, 
                armorDexterityAttribute, 
                armorIntelligenceAttribute
            );

            string armorName2 = "Common Chest Mail";
            int requiredLevel2 = 1;
            ItemSlot slot2 = ItemSlot.Chest;
            ArmorType armorType2 = ArmorType.Mail;
            int armorStrengthAttribute2 = 2;
            int armorDexterityAttribute2 = 0;
            int armorIntelligenceAttribute2 = 0;
            HeroAttribute armorAttribute2 = new(
                armorStrengthAttribute2, 
                armorDexterityAttribute2, 
                armorIntelligenceAttribute2
            );

            Warrior warrior = new(heroName);
            var expected = (warrior.HeroAttributes + armorAttribute2);

            Armor chestPlate = new(armorName, requiredLevel, slot, armorType, armorAttribute);
            Armor chestMail = new(armorName2, requiredLevel2, slot2, armorType2, armorAttribute2);
            warrior.Equip(chestPlate);
            warrior.Equip(chestMail);

            var actual = warrior.TotalAttributes();

            Assert.Equal(expected, actual);
        }
    }

    public class DamageTests
    {
        [Fact]
        public void GetDamage_NoWeaponNoArmor_ShouldReturnScaleDamage()
        {
            string heroName = "Frank";
            double weaponDamage = 1;
            double baseDamageScale = 1;
            double damageAttribute = 5;
            double damageDivider = 100;

            double expected = weaponDamage * (baseDamageScale + (double)(damageAttribute / damageDivider));

            Warrior warrior = new(heroName);
            double actual = warrior.GetDamage();
            
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDamage_WeaponNoArmor_ShouldReturnScaleDamage()
        {
            string heroName = "Frank";
            double baseDamageScale = 1;
            double damageAttribute = 5;
            double damageDivider = 100;

            string weaponName = "Common Axe";
            int weaponRequirementLevel = 1;
            ItemSlot slot = ItemSlot.Weapon;
            WeaponType weaponType = WeaponType.Axe;
            double weaponDamage = 2;

            double expected = weaponDamage * (baseDamageScale + (double)(damageAttribute / damageDivider));

            Warrior warrior = new(heroName);
            Weapon axe = new(weaponName, weaponRequirementLevel, slot, weaponType, weaponDamage);

            warrior.Equip(axe);

            double actual = warrior.GetDamage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDamage_ReplaceWeaponNoArmor_ShouldReturnScaleDamage()
        {
            string heroName = "Frank";
            double baseDamageScale = 1;
            double damageAttribute = 5;
            double damageDivider = 100;

            string weaponName = "Common Axe";
            int weaponRequirementLevel = 1;
            ItemSlot slot = ItemSlot.Weapon;
            WeaponType weaponType = WeaponType.Axe;
            double weaponDamage = 2;

            string weaponName2 = "Common Great Axe";
            int weaponRequirementLevel2 = 1;
            ItemSlot slot2 = ItemSlot.Weapon;
            WeaponType weaponType2 = WeaponType.Axe;
            double weaponDamage2 = 5;

            double expected = weaponDamage2 * (baseDamageScale + (double)(damageAttribute / damageDivider));

            Warrior warrior = new(heroName);
            Weapon axe = new(weaponName, weaponRequirementLevel, slot, weaponType, weaponDamage);
            Weapon greatAxe = new(weaponName2, weaponRequirementLevel2, slot2, weaponType2, weaponDamage2);

            warrior.Equip(axe);
            warrior.Equip(greatAxe);

            double actual = warrior.GetDamage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDamage_EquippedWeaponAndArmor_ShouldReturnScaleDamage()
        {
            string heroName = "Frank";
            double baseDamageScale = 1;
            double damageAttribute = 5;
            double damageDivider = 100;

            string weaponName = "Common Axe";
            int weaponRequirementLevel = 1;
            ItemSlot slot = ItemSlot.Weapon;
            WeaponType weaponType = WeaponType.Axe;
            double weaponDamage = 2;

            string armorName = "Common Chest Plate";
            int armorLevelRequirement = 1;
            ItemSlot armorSlot = ItemSlot.Chest;
            ArmorType armorType = ArmorType.Plate;
            int armorStrengthAttribute = 3;
            int armorDexterityAttribute = 1;
            int armorIntelligenceAttribute = 0;
            HeroAttribute armorAttribute = new(
                armorStrengthAttribute, 
                armorDexterityAttribute, 
                armorIntelligenceAttribute
            );

            double expected = weaponDamage 
                * (baseDamageScale + (double)((damageAttribute + armorStrengthAttribute) / damageDivider));

            Warrior warrior = new(heroName);
            Weapon axe = new(weaponName, weaponRequirementLevel, slot, weaponType, weaponDamage);
            Armor chestPlate = new(armorName, armorLevelRequirement, armorSlot, armorType, armorAttribute);

            warrior.Equip(axe);
            warrior.Equip(chestPlate);

            double actual = warrior.GetDamage();

            Assert.Equal(expected, actual);
        }
    }
}