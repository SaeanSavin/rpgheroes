﻿using RPGHeroes.Attributes;
using RPGHeroes.Items;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Entities.Heroes
{
    public class Rogue : Hero
    {
        private readonly HeroAttribute rogueAttribute = new(2, 6, 1);
        private readonly HeroAttribute rogueLevelupAttribute = new(1, 4, 1);

        public Rogue(string name) : base(name)
        {
            HeroAttributes ??= rogueAttribute;

            ValidWeaponTypes ??= new List<WeaponType>() {
                WeaponType.Dagger,
                WeaponType.Sword
            };
            ValidArmorTypes ??= new List<ArmorType>()
            {
                ArmorType.Leather,
                ArmorType.Mail
            };

        }

        public override string Display()
        {
            return base.Display();
        }

        public override void Equip(Weapon weapon)
        {
            if (Equipment == null) return;

            base.Equip(weapon);
            Equipment[weapon.Slot] = weapon;
        }

        public override void Equip(Armor armor)
        {
            if (Equipment == null) return;

            base.Equip(armor);
            Equipment[armor.Slot] = armor;
        }

        public override double GetDamage()
        {
            return base.GetDamage() * (1 + (double)TotalAttributes().Strength / 100);
        }

        public override void LevelUp()
        {
            if (HeroAttributes == null) return;

            base.LevelUp();
            HeroAttributes += rogueLevelupAttribute;
        }
    }
}
