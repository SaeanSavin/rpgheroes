﻿using RPGHeroes.Attributes;
using RPGHeroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Entities.Heroes
{
    public class Warrior : Hero
    {
        private readonly HeroAttribute warriorAttribute = new(5, 2, 1);
        private readonly HeroAttribute warriorLevelupAttribute = new(3, 2, 1);

        public Warrior(string name) : base(name)
        {
            HeroAttributes ??= warriorAttribute;

            ValidWeaponTypes ??= new List<WeaponType>() {
                WeaponType.Axe,
                WeaponType.Sword,
                WeaponType.Hammer
            };
            ValidArmorTypes ??= new List<ArmorType>()
            {
                ArmorType.Plate,
                ArmorType.Mail
            };
        }

        public override string Display()
        {
            return base.Display();
        }

        public override void Equip(Weapon weapon)
        {
            if (Equipment == null) return;

            base.Equip(weapon);
            Equipment[weapon.Slot] = weapon;
        }

        public override void Equip(Armor armor)
        {
            if (Equipment == null) return;

            base.Equip(armor);
            Equipment[armor.Slot] = armor;
        }

        public override double GetDamage()
        {
            return base.GetDamage() * (1 + (double)TotalAttributes().Strength / 100);
        }

        public override void LevelUp()
        {
            if (HeroAttributes == null) return;

            base.LevelUp();
            HeroAttributes += warriorLevelupAttribute;
        }
    }
}