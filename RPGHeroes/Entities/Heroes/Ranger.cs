﻿using RPGHeroes.Attributes;
using RPGHeroes.Items;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Entities.Heroes
{
    public class Ranger : Hero
    {
        private readonly HeroAttribute rangerAttribute = new(1, 7, 1);
        private readonly HeroAttribute rangerLevelupAttribute = new(1, 5, 1);


        public Ranger(string name) : base(name)
        {
            HeroAttributes ??= rangerAttribute;

            ValidWeaponTypes ??= new List<WeaponType>() {
                WeaponType.Bow
            };
            ValidArmorTypes ??= new List<ArmorType>()
            {
                ArmorType.Mail,
                ArmorType.Leather
            };
        }

        public override string Display()
        {
           return base.Display();
        }

        public override void Equip(Weapon weapon)
        {
            if (Equipment == null) return;

            base.Equip(weapon);
            Equipment[weapon.Slot] = weapon;
        }

        public override void Equip(Armor armor)
        {
            if (Equipment == null) return;

            base.Equip(armor);
            Equipment[armor.Slot] = armor;
        }

        public override double GetDamage()
        {
            if (TotalAttributes() == null) return base.GetDamage();

            return base.GetDamage() * (1 + (double)TotalAttributes().Dexterity / 100);
        }

        public override void LevelUp()
        {
            if (HeroAttributes == null) return;

            base.LevelUp();
            HeroAttributes += rangerLevelupAttribute;
        }
    }
}
