﻿using RPGHeroes.Attributes;
using RPGHeroes.Entities;
using RPGHeroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Entities.Heroes
{
    public class Mage : Hero
    {
        private readonly HeroAttribute mageAttribute = new(1, 1, 8);
        private readonly HeroAttribute mageLevelupAttribute = new(1, 1, 5);

        public Mage(string name) : base(name)
        {
            HeroAttributes ??= mageAttribute;

            ValidWeaponTypes ??= new List<WeaponType>() {
                WeaponType.Staff,
                WeaponType.Wand
            };
            ValidArmorTypes ??= new List<ArmorType>()
            {
                ArmorType.Cloth
            };
        }

        public override double GetDamage() => base.GetDamage() * (1 + (double)TotalAttributes().Intelligence / 100);

        public override string Display()
        {
            return base.Display();
        }

        public override void Equip(Weapon weapon)
        {
            if (Equipment == null) return;

            base.Equip(weapon);
            Equipment[weapon.Slot] = weapon;
        }

        public override void Equip(Armor armor)
        {
            if (Equipment == null) return;

            base.Equip(armor);
            Equipment[armor.Slot] = armor;
        }

        public override void LevelUp()
        {
            if (HeroAttributes == null) return;

            base.LevelUp();
            HeroAttributes += mageLevelupAttribute;
        }
    }
}