﻿using RPGHeroes.Attributes;
using RPGHeroes.Items;
using RPGHeroes.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Entities
{
    public abstract class Hero
    {
        public string Name { get; init; }   
        public int Level { get; private set; }
        public HeroAttribute? HeroAttributes { get; protected set; }
        public Dictionary<ItemSlot, Item?>? Equipment { get; init; }
        public List<WeaponType>? ValidWeaponTypes { get; init; }
        public List<ArmorType>? ValidArmorTypes { get; init; }

        public Hero(string name)
        {
            Name = name;
            Level = 1;
            Equipment = new Dictionary<ItemSlot, Item?>() 
            {
                {ItemSlot.Weapon, null },
                {ItemSlot.Head, null },
                {ItemSlot.Chest, null },
                {ItemSlot.Leggings, null }
            };
        }

        public virtual void LevelUp()
        {
            Console.WriteLine("Level Up!");
            Level++;
        }

        public virtual void Equip(Weapon weapon)
        {
            if (ValidWeaponTypes == null) 
                throw new InvalidWeaponException("Could not equip weapon! Class don't have any valid weapon types!"); ;
            
            if (Level < weapon.RequiredLevel)
                throw new InvalidWeaponException("Could not equip weapon! Hero's level too low!");
            
            if (!ValidWeaponTypes.Contains(weapon.WeaponType))
                throw new InvalidWeaponException("Could not equip weapon! It's invalid weapon type for this class");
            
            Console.WriteLine("Equipped item: " + weapon.Name);
        }

        public virtual void Equip(Armor armor)
        {
            if (ValidArmorTypes == null)
                throw new InvalidArmorException("Could not equip armor! No valid armor types!");

            if (!ValidArmorTypes.Contains(armor.ArmorType))
                throw new InvalidArmorException("Could not equip armor! Not a valid armor type!");

            if (Level < armor.RequiredLevel)
                throw new InvalidArmorException("Could not equip armor! Hero's level too low!");
        }
        
        public virtual double GetDamage()
        {
            if (Equipment == null || Equipment[ItemSlot.Weapon] == null) return 1;
            return ((Weapon)Equipment[ItemSlot.Weapon]).WeaponDamage;
        }

        public virtual HeroAttribute? TotalAttributes()
        {
            if (Equipment == null || HeroAttributes == null) return new HeroAttribute(0,0,0);

            HeroAttribute? armorAttributes = new(0, 0, 0);

            foreach (var armor in Equipment)
            {
                if (armor.Key == ItemSlot.Weapon || (armor.Value) == null || ((Armor)armor.Value).ArmorAttribute == null) continue;
                armorAttributes += ((Armor)armor.Value).ArmorAttribute;
            }
            return HeroAttributes + armorAttributes;
        }

        public virtual string Display()
        {
            StringBuilder builder = new();

            builder.AppendLine($"{Name}");
            builder.AppendLine($"{Level}");
            builder.AppendLine($"Total Strength: {TotalAttributes()?.Strength}");
            builder.AppendLine($"Total Dexterity: {TotalAttributes()?.Dexterity}");
            builder.AppendLine($"Total Intelligence: {TotalAttributes()?.Intelligence}");
            builder.AppendLine($"Damage: {GetDamage()}");

            return builder.ToString();
        }
    }
}
