﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Items
{
    public abstract class Item
    {
        public string? Name { get; init; }
        public int RequiredLevel { get; init; }
        public ItemSlot Slot { get; init; }

        public Item(string? name, int requiredLevel, ItemSlot slot)
        {
            Name = name;
            RequiredLevel = requiredLevel;
            Slot = slot;
        }
    }
}
