﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Items
{
    public class Weapon : Item
    {
        public WeaponType WeaponType { get; init; }

        public double WeaponDamage { get; init; }


        public Weapon(string? name, int requiredLevel, ItemSlot slot, WeaponType weaponType, double weaponDamage) : base(name, requiredLevel, slot)
        {
            WeaponType = weaponType;
            WeaponDamage = weaponDamage;
        }
    }
}
