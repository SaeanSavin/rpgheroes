﻿using RPGHeroes.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Items
{
    public class Armor : Item
    {
        public ArmorType ArmorType { get; init; }
        public HeroAttribute? ArmorAttribute { get; init; }

        public Armor(string? name, int requiredLevel, ItemSlot slot, ArmorType armorType, HeroAttribute armorAttribute) : base(name, requiredLevel, slot)
        {
            ArmorType = armorType;
            ArmorAttribute = armorAttribute;
        }
    }
}
