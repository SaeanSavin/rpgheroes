﻿namespace RPGHeroes.Attributes
{
    public class HeroAttribute
    {
        public int Strength { get; private set; }
        public int Dexterity { get; private set; }
        public int Intelligence { get; private set; }

        public HeroAttribute(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        public static HeroAttribute? operator+(HeroAttribute? lhs, HeroAttribute? rhs)
        {
            if (lhs == null || rhs == null) return null; 

            return new HeroAttribute(
                lhs.Strength + rhs.Strength, 
                lhs.Dexterity + rhs.Dexterity, 
                lhs.Intelligence + rhs.Intelligence
            );
        }
         
        public override string ToString()
        {
            return $"Strenth: {Strength}, Dexterity: {Dexterity}, Intelligence: {Intelligence}";
        }

        public override bool Equals(object? obj)
        {
            if (obj is not HeroAttribute attribute) return false;

            return (attribute.Strength == Strength) &&
                   (attribute.Dexterity == Dexterity) &&
                   (attribute.Intelligence == Intelligence);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Strength, Dexterity, Intelligence);
        }
    }
}
