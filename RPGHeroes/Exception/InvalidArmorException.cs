﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Exception
{
    public class InvalidArmorException : System.Exception
    {
        public InvalidArmorException()
        {}

        public InvalidArmorException(string? message) : base(message)
        {}

        public InvalidArmorException(string? message, System.Exception? innerException) : base(message, innerException)
        {}
    }
}
