﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Exception
{
    [Serializable]
    public class InvalidWeaponException : System.Exception
    {
        public InvalidWeaponException() { }

        public InvalidWeaponException(string message): base(message) {}

        public InvalidWeaponException(string message, System.Exception inner) : base(message, inner) {}
    }
}
