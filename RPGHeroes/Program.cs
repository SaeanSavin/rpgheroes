﻿using RPGHeroes.Entities;
using RPGHeroes.Entities.Heroes;
using RPGHeroes.Items;
using RPGHeroes.Attributes;

namespace RPGHeroes
{
    public class Program
    {
        static void Main(string[] args)
        {
            Hero mage = new Mage("Oliver");
            Ranger ranger = new("John");

            mage.LevelUp();
            mage.LevelUp();

            Weapon staff = new("Staff", 1, ItemSlot.Weapon, WeaponType.Staff, 10);
            Weapon bow = new("Short bow", 1, ItemSlot.Weapon, WeaponType.Bow, 15);

            Armor hood = new("Hood", 1, ItemSlot.Head, ArmorType.Cloth, new HeroAttribute(1, 3, 2));
            Armor chestPlate = new("Chestplate", 3, ItemSlot.Chest, ArmorType.Plate, new HeroAttribute(5, 2, 0));
            Armor mail = new("Mail", 1, ItemSlot.Head, ArmorType.Mail, new HeroAttribute(2, 6, 1));

            ranger.LevelUp();

            ranger.Equip(bow);
            //ranger.Equip(hood);
            ranger.Equip(mail);

            Console.WriteLine("Damage: " + ranger.GetDamage());

            //mage.Equip(staff);
            
            //mage.Equip(hoodArmor);
            //mage.Equip(chestArmor);
            //Console.WriteLine(mage.GetDamage());

            Console.WriteLine("Hello Hero");
        }
    }
}